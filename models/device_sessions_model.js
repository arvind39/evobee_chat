
const db_sql = require('../config/database.js');
const common_helper = require('../helper/common_helper.js');

let table = 'device_sessions';

async function get_data_by_params(search_data) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0'`;
       
        if(!common_helper.is_empty(search_data.user_id))
        {
            sql_query += `AND user_id ='${search_data.user_id}' `;
        }
        if(!common_helper.is_empty(search_data.user_type))
        {
            sql_query += `AND user_type ='${search_data.user_type}' `;
        }
        if(!common_helper.is_empty(search_data.socket_id))
        {
            sql_query += `AND socket_id ='${search_data.socket_id}' `;
        }

        if(!common_helper.is_empty(search_data.socket_status))
        {
            sql_query += `AND socket_status ='${search_data.socket_status}' `;
        }
        if(!common_helper.is_empty(search_data.access_token))
        {
            sql_query += `AND access_token ='${search_data.access_token}' `;
        }
        
        sql_query += `LIMIT 0,1`;

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function get_device_token_data(user_id,user_type,device_type,socket_status=1) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE user_id='${user_id}' AND user_type='${user_type}' AND device_type='${device_type}' AND socket_status='${socket_status}' AND is_deleted = '0' ORDER BY id desc LIMIT 0,1`;
   // console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function get_device_data_by_access_token(access_token) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE access_token='${access_token}' AND status='1' AND is_deleted = '0' ORDER BY id desc LIMIT 0,1`;
    //console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function get_device_data_by_socket_id(socket_id) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE socket_id='${socket_id}'  AND status='1' AND is_deleted = '0' ORDER BY id desc LIMIT 0,1`;
    //console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function save(insert_data,device_session_id = '') 
{
   if(common_helper.is_empty(device_session_id))
   {
        let sql_query = common_helper.get_Insert_Query(insert_data,table) ;
        //console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
   else
   {
        let sql_query = common_helper.get_Update_Query(insert_data,table,table+'.id ='+device_session_id) ;
        //console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
}
   

module.exports = {
    get_device_token_data,
    save,
    get_device_data_by_access_token,
    get_device_data_by_socket_id,
    get_data_by_params,
}