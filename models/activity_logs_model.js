const db_sql = require('../config/database.js');
const common_helper = require('../helper/common_helper.js');
let table = 'activity_log' ;


async function create(insert_data,activity_log_id = '') 
{
   if(common_helper.is_empty(activity_log_id))
   {    
        insert_data.created_at = common_helper.get_current_date() ;
        let sql_query = common_helper.get_Insert_Query(insert_data,table) ;
        //console.log(sql_query,'log sql_query') ;
        return db_sql.query(sql_query);
    }
   else
   {        
        insert_data.updated_at = common_helper.get_current_date() ;
        let sql_query = common_helper.get_Update_Query(insert_data,table,table+'.id ='+activity_log_id) ;
        //console.log(sql_query,'log sql_query') ;
        return db_sql.query(sql_query);
    }
}






module.exports = {
    create,
  }