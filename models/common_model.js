const sql = require('../config/database.js');
const common_helper = require('../helper/common_helper.js');
const CONSTANTS = require('../config/constants.js');


function get_state_list_by_country_id (country_id) 
{
    return new Promise(resolve => {
       
        let sql_query = `SELECT *  FROM states WHERE country_id = '${country_id}' AND is_deleted = '0'  order by name ASC`; 
        sql.query(sql_query) .then( result => {
            //console.log(parseInt(result.length),'result');
            resolve(result);
        }).catch( err => {
            reject(err) ;
        });
      });
}

function get_city_list_by_state_id (state_id) 
{
    return new Promise(resolve => {
       
        let sql_query = `SELECT *  FROM cities WHERE state_id = '${state_id}' AND is_deleted = '0'  order by name ASC`; 
        sql.query(sql_query) .then( result => {
            //console.log(parseInt(result.length),'result');
            resolve(result);
        }).catch( err => {
            reject(err) ;
        });
      });
}

async function get_user_data_by_id_type (user_id,user_type) 
{
    let sql_query = '' ;
    if( user_type == CONSTANTS.IS_USER )
    {
        sql_query = `SELECT u.user_id as id,CONCAT(u.user_firstname," ",u.user_lastname) as name,u.user_firstname as first_name,u.user_lastname as last_name,u.user_birthday as birthday,u.user_emailid as email,u.user_phone as phone,u.profile_image as image  FROM user as u WHERE u.user_id = '${user_id}' AND is_deleted = '0'  limit 0,1`;    
      
        
    }
    if( user_type == CONSTANTS.IS_BUSINESS )
    {
        sql_query = `SELECT b.business_id as id,b.parent_id as parent_id,b.business_name as name,b.business_email as email,b.business_phone as phone,b.business_address as address,b.business_logo as image FROM business as b WHERE b.business_id = '${user_id}' AND is_deleted = '0'  limit 0,1`;
    }
    
    return sql.query(sql_query);   
              
        
    
}



module.exports = {
    get_state_list_by_country_id,
    get_city_list_by_state_id,
    get_user_data_by_id_type,
  }