'use strict';

var express = require("express");
var app = express();
var http = require("http").createServer(app);
const multer = require("multer");
var fs = require("fs");
var mime = require("mime");
var cors = require('cors')

app.use(cors()) ;

require('dotenv').config() ;
const port = process.env.PORT || process.env.SERVER_PORT ;

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: "50mb"}));


app.use(function (request, result, next) {
	result.setHeader("Access-Control-Allow-Origin", "*");
	result.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});




app.get("/", function (req, res) {
	res.json({ message: "Welcome to Evobee Chat Application." });
});

const chatRouter = require(`./chat/routes/chat.js`);
app.use('/chat', chatRouter);

const CONSTANTS = require('./config/constants.js');

const socketChatService = require('./chat/services/socket_chat_service.js');
var io = require("socket.io")(http);
	io.on("connection", function (socket) {

			console.log("User Socket Id: ",  socket.id);
			socket.on("user_connect", function (postData) {
				if(socket.id == postData.socket_id )
				{
					socketChatService.userConnectWithSocket(postData).then( (response)=>{
						socket.broadcast.emit('user_connected',response);
						socket.broadcast.emit('is_user_live',response);
						console.log(response,'user_connected');
						console.log(socket.id,'Connect socket id');
					});
				}
			});

			socket.on("is_user_live", function (postData) {
				socketChatService.is_user_live(postData).then( (response)=>{
					socket.broadcast.emit('is_user_live',response);
					console.log(response,'is_user_live');
				});
			});

			socket.on('send_message',(message) => {
				console.log(message,'message');
				socket.broadcast.emit('send_to_message',message);
				
				
			});
			socket.broadcast.emit('send_to_message','hello');
			socket.on("disconnect", (reason) => {

				socketChatService.userDisconnectWithSocket({socket_id:socket.id}).then( (response)=>{
					if(response.status)
					{ 
						socket.broadcast.emit('is_user_live',response);
					}
					console.log(response,'discconnecct response');
					console.log(socket.id,'Discconnecct socket id');
					
				});
				
				console.log(reason,'reason');
			});

	});



http.listen(port, () => {
    console.log(`Server is running on port http://localhost:${port}`);
  });