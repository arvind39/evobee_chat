const chat_message_model = require(`../models/chat_message_model.js`);
const common_helper = require(`../../helper/common_helper.js`);

async function count_unseen_msg(search_data) 
{       
    return new Promise( async (resolve,reject)=>{

        chat_message_model.get_data(search_data,'','').then( (res) => {
            if(parseInt(res.length))
            {
                resolve(parseInt(res.length));
            }
            else
            {
                resolve(0);
            }
        }).catch( (err)=>{
              console.log(err,'db error')
              resolve(0);
        });

     });
}

async function get_data(search_data,offset='',per_page='') 
{       
    return new Promise( async (resolve,reject)=>{

        chat_message_model.get_data(search_data,offset,per_page).then( (res) => {
            if(parseInt(res.length))
            {
                resolve(res);
            }
            else
            {
                resolve([]);
            }
        }).catch( (err)=>{
              console.log(err,'db error')
              resolve(0);
        });

     });
}

async function insert_update(insert_data,msg_id='') 
{       
    return new Promise( async (resolve,reject)=>{

        chat_message_model.save(insert_data,msg_id).then( (msg_res) => {
            let chat_id = msg_res.insertId ; 
            resolve(chat_id);
        }).catch( (err)=>{
              console.log(err,'db error')
              resolve(0);
        });

     });
}

async function files_insert(insert_data,json_string) 
{      
    let insert_ids = [] ; 
    return new Promise( async (resolve,reject)=>{

        let object_data =JSON.parse(json_string);
        console.log(object_data,'object_data');
        for (let value of Object.values(object_data)) 
        {
            insert_data.msg_type = value.file_type ;
            insert_data.message  = value.file ;
            let insert_id = await insert_update(insert_data);
            insert_ids.push(insert_id);
            
        }
        resolve(insert_ids);

     });
}

async function save_chat_data_and_get(postData,insert_data) 
{      
    let insert_ids = [] ; 
    return new Promise( async (resolve,reject)=>{

        if(!common_helper.is_empty(postData.files_data))
        {
            insert_ids = await files_insert(insert_data,postData.files_data);
        }
        if(!common_helper.is_empty(postData.message))
        {   
            insert_data.msg_type = postData.msg_type ;
            insert_data.message  = postData.message ;
            let res = await insert_update(insert_data);
            insert_ids.push(res);
        }
        console.log(insert_ids,'insert_ids');

          let result = await get_data({msg_id:insert_ids});

        for (let value of Object.values(result)) 
        {
            value.sender_info = await common_helper.get_user_data_by_id_type(value.sender_id,value.sender_type) ;
            value.receiver_info = await common_helper.get_user_data_by_id_type(value.receiver_id,value.receiver_type) ;
        }
        let user_chat_info = await common_helper.get_user_data_by_id_type(insert_data.sender_id,insert_data.sender_type) ;
        resolve({ chat_list:result,user_chat_info:user_chat_info,request_id:insert_data.request_id});

     });
}






module.exports = {
    count_unseen_msg,
    files_insert,
    insert_update,
    save_chat_data_and_get,
    get_data,
}