const CONSTANTS = require('../../config/constants.js');
const Common_helper = require(`../../helper/common_helper.js`);
const Common_model = require(`../../models/common_model.js`);
const Device_sessions_model = require(`../../models/device_sessions_model.js`);
const chat_request_model = require(`../models/chat_request_model.js`);
const chat_message_model = require(`../models/chat_message_model.js`);
const chat_helper = require(`../helper/chat_helper.js`);
const { Validator } = require('node-input-validator');

const moment = require('moment'); 

const sql = require('../../config/database.js');

async function userConnectWithSocket (postData) 
 {
     
    return new Promise( (resolve,reject)=>{
        let response = Common_helper.get_response_variable();
        let result = Device_sessions_model.get_device_data_by_access_token(postData._token);
        result.then((res)=>{
            let row_count = parseInt(res.length);
            if(row_count)
            {
                let row_data = res[0];
                Device_sessions_model.save({socket_id:postData.socket_id,socket_status:1},row_data.id);

                let query_result = Device_sessions_model.get_device_data_by_access_token(postData._token);
                query_result.then((device_res)=>{
                    response = Common_helper.get_response_variable();
                    response.status = 1 ;
                    response.data = device_res[0] ;
                    response.message = 'User Connected';
                    resolve(response) ;

                });

               
            }
            else
            {
                response = Common_helper.get_response_variable();
                response.message = 'User did not connect' ;
                resolve(response) ;
            }
            
        }).catch( (err)=>{
            response = Common_helper.get_response_variable();
            response.message = err ;
            resolve(response) ;
        });
   });
   
};

async function userDisconnectWithSocket (postData) 
 {
     
    return new Promise( (resolve,reject)=>{
        let response = Common_helper.get_response_variable();
        let result = Device_sessions_model.get_device_data_by_socket_id(postData.socket_id);
        result.then((res)=>{
            let row_count = parseInt(res.length);
            if(row_count)
            {
                let row_data = res[0];
                Device_sessions_model.save({socket_id:'',socket_status:0},row_data.id);

                let query_result = Device_sessions_model.get_device_data_by_access_token(postData.socket_id);
                query_result.then((device_res)=>{

                    let res_count = parseInt(device_res.length); 
                    if(res_count)
                    {
                        response = Common_helper.get_response_variable();
                        response.status = 1 ;
                        response.data = device_res[0] ;
                        response.message = 'User Disconnected ';
                        resolve(response) ;
                    }
                    else
                    {
                        response = Common_helper.get_response_variable();
                        response.status = 1 ;
                        row_data.socket_status = 0 ;
                        response.data = row_data ;
                        response.message = 'User Disconnected ';
                        resolve(response) ;
                    }
                }).catch( (err)=>{  
                    console.log(err,'errsss');

                });
            }
            else
            {
                response = Common_helper.get_response_variable();
                response.message = 'User did not disconnect' ;
                resolve(response) ;
            }
            
        }).catch( (err)=>{
            response = Common_helper.get_response_variable();
            response.message = err ;
            resolve(response) ;
        });
   });
   
};

async function is_user_live (postData) 
 {
     
    return new Promise( (resolve,reject)=>{
        let response = Common_helper.get_response_variable();
        let result = Device_sessions_model.get_data_by_params(postData);
        result.then((res)=>{
            let row_count = parseInt(res.length);
            if(row_count)
            {
                let row_data = res[0];
                response = Common_helper.get_response_variable();
                response.status = 1 ;
                response.data   = row_data ;
                resolve(response) ;
            }
            else
            {
                response = Common_helper.get_response_variable();
                response.message = 'No user found' ;
                resolve(response) ;
            }
            
        }).catch( (err)=>{
            response = Common_helper.get_response_variable();
            response.message = err ;
            resolve(response) ;
        });
   });
   
};

async function request (req) 
 {
     
    return new Promise( (resolve,reject)=>{
        console.log(req);
        let postData = req.body;
    let rules_field = {
                        to_user_id: 'required',
                        to_user_type: 'required',
                        ip_address: 'required',
                        latitude: 'required',
                        longitude: 'required',
                        } ;
    const validate = new Validator(postData, rules_field);
      validate.check().then((matched) => {  
        if (matched)
        { 
            let insert_data = {
                                request_type:2,
                                from_user_id:req.client_data.user_id,
                                from_user_type:req.client_data.user_type,
                                to_user_id:postData.to_user_id,
                                to_user_type:postData.to_user_type,
                                ip_address:postData.ip_address,
                                latitude:postData.latitude,
                                longitude:postData.longitude,
                              };
           
            chat_request_model.get_chat_request_id(insert_data.from_user_id,insert_data.from_user_type,insert_data.to_user_id,insert_data.to_user_type).then( (reslut)=>{
                    let request_id = '' ;
                    if(parseInt(reslut.length))
                    {
                        let row_data  =  reslut[0];
                        request_id = row_data.request_id ;
                    }
                    console.log(request_id,'request_id');

                    chat_request_model.save(insert_data,request_id).then( (res)=>{
                        console.log(res,'ress');
                        console.log(res.insertId,'insertId');

                        let response = Common_helper.get_response_variable();    
                        response.status = 1 ; 
                        response.data = {request_id:Common_helper.is_empty(request_id)?res.insertId:request_id} ; 
                        response.message = 'Request id has been generated' ; 
                        resolve(response) ;     
                    });
            }).catch( (err)=>{
                console.log(err,'error');
            });    
            
        }
        else
        {   
            let response = Common_helper.get_response_variable();             
            let errors_data = Common_helper.convert_to_empty_errors(rules_field,validate.errors);
            response.status  = 0 ;
            response.message = '' ;
            response.error = errors_data ;
            resolve(response) ;
        }
    });
   });
}
async function recentChatList (req) 
 {
     
    return new Promise( (resolve,reject)=>{
        //console.log(req);
        let postData = req.query;
        console.log(postData,'postData');
        let page = !Common_helper.is_empty(postData.page) ? postData.page : 1 ;
        let per_page = !Common_helper.is_empty(postData.per_page) ? parseInt(postData.per_page) : parseInt(10) ;
        let offset   = (parseInt(page-1)*parseInt(per_page)) ; 

        let search_data = postData ;
            search_data.to_user_id = req.client_data.user_id ;
            search_data.to_user_type = req.client_data.user_type ;
            search_data.in_chat = 0 ;
            
        

            let records = chat_message_model.get_recent_chat_list(search_data,'','');
            let total_rows = 0 ;
            records.then( (re)=>{
                total_rows = parseInt(re.length) ; 
            });

            chat_message_model.get_recent_chat_list(search_data,offset,per_page).then( async(result)=>{
            let row_count = parseInt(result.length) ;
            
            if(row_count)
            {   
                for (let value of Object.values(result)) 
                {
                    let human_time = moment(moment(value.send_date).format('ddd MMM DD YYYY HH:mm:s')).toNow(true);
                       value.human_time = human_time;
                    value.sender_info = await Common_helper.get_user_data_by_id_type(value.sender_id,value.sender_type) ;
                    value.receiver_info = await Common_helper.get_user_data_by_id_type(value.receiver_id,value.receiver_type) ;
                    value.unseen_count = await chat_helper.count_unseen_msg({request_id:value.request_id,is_seen:0});
                }

                let response = Common_helper.get_response_variable();    
                response.status = 1 ; 
                response.message = 'Record found' ; 
                response.page  = page ; 
                response.per_page  = per_page ; 
                response.total_rows  = total_rows ; 
                response.total_number_of_page  = Math.ceil((total_rows/per_page),0) ; 
                response.data  = result ; 
                //response.human  = Common_helper.millisecondsToStr(1379426880000) ; 
                resolve(response) ;   

            }
            else
            {
                let response = Common_helper.get_response_variable();    
                response.status = 0 ; 
                response.message = 'No record found' ; 
                response.page  = page ; 
                response.per_page  = per_page ; 
                response.total_rows  = 0 ; 
                response.total_number_of_page  =0;
                resolve(response) ;   
            }
             

        });
    
   });
}

async function sendMessage (req) 
 {
     
    return new Promise( (resolve,reject)=>{
        //console.log(req);
       // console.log(req.body);
        let postData = req.body;
    let rules_field = {
                        request_id: 'required',
                        //message: 'required',
                        msg_type: 'required',
                        } ;
     if(Common_helper.is_empty(postData.files_data))
     {
        rules_field.message = 'required';
     }
    const validate = new Validator(postData, rules_field);
      validate.check().then((matched) => {  
        if (matched)
        { 
            
            
                    let insert_data = {
                        request_id:postData.request_id,
                        message:postData.message,
                        msg_type:postData.msg_type,
                        }; 
                    let session_data = req.client_data;
                    chat_request_model.check_chat_request_id(postData.request_id,session_data.user_id,session_data.user_type).then( (result) => {

                        if(parseInt(result.length))
                        { 
                            let request_data = result[0];
                            let insert_data = {
                                request_id:request_data.request_id,
                                sender_id:session_data.user_id,
                                sender_type:session_data.user_type,
                                receiver_id:request_data.to_user_id,
                                receiver_type:request_data.to_user_type,
                            };
                            if(insert_data.sender_id == request_data.to_user_id && insert_data.sender_type == request_data.to_user_type )
                            {
                                insert_data.receiver_id = request_data.from_user_id ;
                                insert_data.receiver_type = request_data.from_user_type ;
                            }
                            console.log(insert_data,'insert_data');
                            chat_helper.save_chat_data_and_get(postData,insert_data,postData.files_data).then( (data)=>{
                                
                                let response = Common_helper.get_response_variable();    
                                    response.status = 1 ; 
                                    response.message = 'Message send successfully' ; 
                                    response.data = data ; 
                                 resolve(response) ;
                            });

                            
                        }
                        else
                        {
                            let response = Common_helper.get_response_variable();    
                                response.status = 0 ; 
                                response.message = 'Request Id not valid' ; 
                                resolve(response) ;
                        }
                        

                    });
                  
            
        }
        else
        {   
            let response = Common_helper.get_response_variable();             
            let errors_data = Common_helper.convert_to_empty_errors(rules_field,validate.errors);
            response.status  = 0 ;
            response.message = '' ;
            response.error = errors_data ;
            resolve(response) ;
        }
    });
   });
}




async function messageList(req) 
 {
     
    return new Promise( (resolve,reject)=>{

        let postData = req.body;
        let rules_field = {
            request_id: 'required',
            page: 'required',
            per_page: 'required',
            } ;
const validate = new Validator(postData, rules_field);
validate.check().then((matched) => {  
    if (matched)
    {   
        chat_request_model.get_data_by_params({request_id:postData.request_id,receiver_type:2,status:1,in_chat:0}).then( (request_res)=>{
            console.log(request_res,'request_res');
            console.log(parseInt(request_res),'count request_res');
            if(parseInt(request_res.length))
            { 
                let chat_reuest_data = request_res[0];

                let page = !Common_helper.is_empty(postData.page) ? postData.page : 1 ;
                let per_page = !Common_helper.is_empty(postData.per_page) ? parseInt(postData.per_page) : parseInt(10) ;
                let offset   = (parseInt(page-1)*parseInt(per_page)) ; 
                let search_data = {
                                request_id:postData.request_id,
                                } ;

                let records = chat_message_model.get_data(search_data,'','');
                let total_rows = 0 ;
                records.then( (re)=>{
                    total_rows = parseInt(re.length) ; 
                });

            chat_message_model.get_data(search_data,offset,per_page).then( async (result)=>{
                let row_count = parseInt(result.length) ;
                
                if(row_count)
                {
                     let response_data = [] ;
                    for (let value of Object.values(result)) 
                    { 
                       let human_time = moment(moment(value.send_date).format('ddd MMM DD YYYY HH:mm:s')).toNow(true);
                       value.human_time = human_time;
                       value.sender_info = await Common_helper.get_user_data_by_id_type(value.sender_id,value.sender_type) ;
                       value.receiver_info = await Common_helper.get_user_data_by_id_type(value.receiver_id,value.receiver_type) ;
                       response_data.push(value);
                    }
                    result.sort((firstItem, secondItem) => firstItem.id - secondItem.id);
                    let user_chat_info = await Common_helper.get_user_data_by_id_type(chat_reuest_data.from_user_id,chat_reuest_data.from_user_type) ;
                    //console.log(response_data,'response_data');
                    let response = Common_helper.get_response_variable();    
                    response.status = 1 ; 
                    response.message = 'Record found' ; 
                    response.page  = page ; 
                    response.per_page  = per_page ; 
                    response.total_rows  = total_rows ; 
                    response.total_number_of_page  = Math.ceil((total_rows/per_page),5) ; 
                    response.data  = { chat_list:result,user_chat_info:user_chat_info,request_id:postData.request_id} ;
                    resolve(response) ;   

                }
                else
                {
                    let response = Common_helper.get_response_variable();    
                    response.status = 0 ; 
                    response.message = 'No record found' ; 
                    response.page  = page ; 
                    response.per_page  = per_page ; 
                    response.total_rows  = 0 ; 
                    response.total_number_of_page  =0;
                    resolve(response) ;   
                }
            });
            }
            else
            {
                let response = Common_helper.get_response_variable();    
                response.status = 0 ; 
                response.message = 'Request id invalid' ; 
                resolve(response) ;
            }
        });
            

        


    }
    else
    {   
        let response = Common_helper.get_response_variable();             
        let errors_data = Common_helper.convert_to_empty_errors(rules_field,validate.errors);
        response.status  = 0 ;
        response.message = '' ;
        response.error = errors_data ;
        resolve(response) ;
    }
});
    
   });
}




const fs = require('fs');
const mime = require('mime');

const uploadImage = async (req, res, next) => {
  // to declare some path to store your converted image
  var matches = req.body.base64image.match(/^data:([A-Za-z-+/]+);base64,(.+)$/),
  response = {};
   
  if (matches.length !== 3) {
  return new Error('Invalid input string');
  }
   
  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');
  let decodedImg = response;
  let imageBuffer = decodedImg.data;
  let type = decodedImg.type;
  let extension = mime.extension(type);
  let fileName = Common_helper.get_current_date()+" file." + extension;
  console.log(response,'response');
  try {
    fs.writeFileSync("./images/" + fileName, imageBuffer, 'utf8');
    console.log('succcess');
    return res.json({"status":"success"});
  } catch (e) {
    console.log(e,'err');
     next(e);
  }
  }


  async function seenmessage(req) 
 {
     let postData = req.body;
    return new Promise( (resolve,reject)=>{

        let postData = req.body;
        let rules_field = {
            message_id: 'required',
            is_seen: 'required',
            } ;
const validate = new Validator(postData, rules_field);
    validate.check().then((matched) => {   
    if (matched)
    {   

        try{

            let message_id  =  Common_helper.extractValue(JSON.parse(postData.message_id),'id');
            let ids = message_id.join(',');
            chat_message_model.save_is_seen_status({is_seen:postData.is_seen},ids).then( (res)=> {
                let response = Common_helper.get_response_variable(); 
                response.status  = 1 ;
                response.message = 'Seen successfully' ;
                resolve(response) ;
            });
         

        }
        catch(err)
        {
            let response = Common_helper.get_response_variable();   
            response.message = 'Message id is invalid format' ;
            resolve(response) ;
        }
    }
    else
    {   
        let response = Common_helper.get_response_variable();             
        let errors_data = Common_helper.convert_to_empty_errors(rules_field,validate.errors);
        response.status  = 0 ;
        response.message = '' ;
        response.error = errors_data ;
        resolve(response) ;
    }
});
    
   });
}


   
module.exports = {
    userConnectWithSocket,
    userDisconnectWithSocket,
    is_user_live,
    request,
    recentChatList,
    sendMessage,
    messageList,
    uploadImage,
    seenmessage,
    
}