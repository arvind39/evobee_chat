const CONSTANTS = require('../../config/constants.js');
const Common_helper = require(`../../helper/common_helper.js`);
const Common_model = require(`../../models/common_model.js`);
const Device_sessions_model = require(`../../models/device_sessions_model.js`);
const { Validator } = require('node-input-validator');

const sql = require('../../config/database.js');

async function chatList (req) {
    //console.log(req,'req');
    let postData  =  req.body ;
    return new Promise( (resolve,reject)=>{
        let response = Common_helper.get_response_variable();
        resolve(response) ;
   }); 
};


async function userList (req) {
    //console.log(req,'req');
    let postData  =  req.body ;
    return new Promise( (resolve,reject)=>{
    let dd=  sql.query("SELECT * FROM `user` LIMIT 10 ");
	dd.then( result=> {
        let response = Common_helper.get_response_variable();
        response.status = 1 ;
        response.data = result ;
        resolve(response) ;
	});
        
   }); 
};

module.exports = {
    chatList,
    userList,
}