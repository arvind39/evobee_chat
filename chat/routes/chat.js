const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/auth.js');
const chatService = require('../services/chat_service.js');
const socketChatService = require('../services/socket_chat_service.js');



  router.post('/send',checkAuth, async function(req, res, next) {
    try {
         res.json(await socketChatService.sendMessage(req));
    } catch (err) {
         console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });


router.get('/recent', checkAuth, async function(req, res, next) {
    try {
         res.json(await socketChatService.recentChatList(req));
    } catch (err) {
      console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });

  router.post('/messagelist',checkAuth, async function(req, res, next) {
    try {
         res.json(await socketChatService.messageList(req));
    } catch (err) {
         console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });

  router.post('/is_user_live', async function(req, res, next) {
    try {
         res.json(await socketChatService.is_user_live(req.body));
    } catch (err) {
         console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });

  

  router.get('/userlist', async function(req, res, next) {
    try {
       
         res.json(await chatService.userList(req));
    } catch (err) {
         console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });

  router.post('/request',checkAuth, async function(req, res, next) {
    try {
       
         res.json(await socketChatService.request(req));
    } catch (err) {
          console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });

  router.post('/seenmessage',checkAuth, async function(req, res, next) {
    try {
         res.json(await socketChatService.seenmessage(req));
    } catch (err) {
          console.error(`Some thing went to wrong `, err.message);
      next(err);
    }
  });


module.exports = router;