
const db_sql = require('../../config/database.js');
const common_helper = require('../../helper/common_helper.js');

let table = 'chat_message';

async function get_data_by_params(search_data) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0'`; 

    if(!common_helper.is_empty(search_data.id))
    {
        sql_query += `AND id ='${search_data.id}' `;
    }
    if(!common_helper.is_empty(search_data.parent_id))
    {
        sql_query += `AND parent_id ='${search_data.parent_id}' `;
    }
    if(!common_helper.is_empty(search_data.request_id))
    {
        sql_query += `AND request_id ='${search_data.request_id}' `;
    }

    if(!common_helper.is_empty(search_data.is_seen))
    {
        sql_query += `AND is_seen ='${search_data.is_seen}' `;
    }
    if(!common_helper.is_empty(search_data.is_reply))
    {
        sql_query += `AND is_reply ='${search_data.is_reply}' `;
    }       
        sql_query += `LIMIT 0,1`;

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function get_data(search_data,page,per_page) 
{
    let sql_query = `SELECT ch_msg.*,DATE_FORMAT(ch_msg.created_at,"%m-%d-%Y %H:%i:%s") as send_date  FROM ${table} ch_msg WHERE  ch_msg.is_deleted = '0'`;
       
        if(!common_helper.is_empty(search_data.parent_id))
        {
            sql_query += `AND ch_msg.parent_id ='${search_data.parent_id}' `;
        }
        if(!common_helper.is_empty(search_data.msg_id))
        {
            sql_query += `AND ch_msg.id IN (${search_data.msg_id}) `;
        }

        if(!common_helper.is_empty(search_data.request_id))
        {
            sql_query += `AND ch_msg.request_id ='${search_data.request_id}' `;
        }

        if(!common_helper.is_empty(search_data.is_seen))
        {
            sql_query += `AND ch_msg.is_seen ='${search_data.is_seen}' `;
        }
        if(!common_helper.is_empty(search_data.is_reply))
        {
            sql_query += `AND ch_msg.is_reply ='${search_data.is_reply}' `;
        }
       
        sql_query += `ORDER BY ch_msg.id DESC `;

       
        if(!common_helper.is_empty(per_page))
        {
            sql_query += `LIMIT ${page},${per_page}`;
        }
        
       

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

 

async function save(insert_data,msg_id = '') 
{
   if(common_helper.is_empty(msg_id))
   {    
        insert_data.created_at = common_helper.get_current_date();
        let sql_query = common_helper.get_Insert_Query(insert_data,table) ;
        console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
   else
   {    insert_data.modified_at = common_helper.get_current_date();
        let sql_query = common_helper.get_Update_Query(insert_data,table,table+'.id ='+msg_id) ;
        console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
}

async function get_recent_chat_list(search_data,page,per_page) 
{
    let sql_query = `SELECT msg.*,chr.from_user_id,chr.from_user_type,chr.to_user_id,chr.to_user_type,chr.in_chat,DATE_FORMAT(msg.created_at,"%m-%d-%Y %H:%i:%s") as send_date  FROM ${table} msg `;

        sql_query += `JOIN chat_requests chr ON msg.request_id = chr.request_id `;

        if(!common_helper.is_empty(search_data.user_name))
        {
            sql_query += ` JOIN user u ON msg.sender_id = u.user_id  `;
        }

        sql_query += ` WHERE chr.request_type = '2' AND  msg.is_deleted = '0'`;

        sql_query += `AND msg.id IN(SELECT MAX(id) FROM ${table} GROUP by request_id)`;
        
        if(!common_helper.is_empty(search_data.user_name))
        {
            sql_query += `  AND (u.user_firstname LIKE '%${search_data.user_name}%' OR u.user_lastname LIKE '%${search_data.user_name}%'  ) `;
        }

        if(!common_helper.is_empty(search_data.message))
        {
            sql_query += `AND msg.message LIKE '%${search_data.message}%' `;
        }


        if(!common_helper.is_empty(search_data.in_chat))
        {
            sql_query += `AND chr.in_chat ='${search_data.in_chat}' `;
        }
        if(!common_helper.is_empty(search_data.from_user_id))
        {
            sql_query += `AND chr.from_user_id ='${search_data.from_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.from_user_type))
        {
            sql_query += `AND chr.from_user_type ='${search_data.from_user_type}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_id))
        {
            sql_query += `AND chr.to_user_id ='${search_data.to_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_type))
        {
            sql_query += `AND chr.to_user_type ='${search_data.to_user_type}' `;
        }

        if(!common_helper.is_empty(search_data.parent_id))
        {
            sql_query += `AND msg.parent_id ='${search_data.parent_id}' `;
        }
        if(!common_helper.is_empty(search_data.request_id))
        {
            sql_query += `AND msg.request_id ='${search_data.request_id}' `;
        }

        if(!common_helper.is_empty(search_data.is_seen))
        {
            sql_query += `AND msg.is_seen ='${search_data.is_seen}' `;
        }
        if(!common_helper.is_empty(search_data.is_reply))
        {
            sql_query += `AND msg.is_reply ='${search_data.is_reply}' `;
        }

        sql_query += `ORDER BY msg.id DESC `;

        if(!common_helper.is_empty(per_page))
        {
            sql_query += `LIMIT ${page},${per_page}`;
        }

    //console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}
   

async function save_is_seen_status(insert_data,msg_id = '') 
{
      insert_data.modified_at = common_helper.get_current_date();
      let where_condition = `${table}.id IN (${msg_id})`;
        let sql_query = common_helper.get_Update_Query(insert_data,table,where_condition) ;
        console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
}

module.exports = { 
    save,
    get_data_by_params,
    get_data,
    get_recent_chat_list,
    save_is_seen_status,

}