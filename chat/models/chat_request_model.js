
const db_sql = require('../../config/database.js');
const common_helper = require('../../helper/common_helper.js');

let table = 'chat_requests';

async function get_data_by_params(search_data) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0'`;
       
        if(!common_helper.is_empty(search_data.request_id))
        {
            sql_query += `AND request_id ='${search_data.request_id}' `;
        }
        if(!common_helper.is_empty(search_data.request_type))
        {
            sql_query += `AND request_type ='${search_data.request_type}' `;
        }
        if(!common_helper.is_empty(search_data.from_user_id))
        {
            sql_query += `AND from_user_id ='${search_data.from_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.from_user_type))
        {
            sql_query += `AND from_user_type ='${search_data.from_user_type}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_id))
        {
            sql_query += `AND to_user_id ='${search_data.to_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_type))
        {
            sql_query += `AND to_user_type ='${search_data.to_user_type}' `;
        }
        if(!common_helper.is_empty(search_data.status))
        {
            sql_query += `AND status ='${search_data.status}' `;
        }

        if(!common_helper.is_empty(search_data.in_chat))
        {
            sql_query += `AND in_chat ='${search_data.in_chat}' `;
        }
        
        sql_query += `LIMIT 0,1`;

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function get_data(search_data,page,per_page) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0'`;
       
        if(!common_helper.is_empty(search_data.request_id))
        {
            sql_query += `AND request_id ='${search_data.request_id}' `;
        }
        if(!common_helper.is_empty(search_data.request_type))
        {
            sql_query += `AND request_type ='${search_data.request_type}' `;
        }

        if(!common_helper.is_empty(search_data.from_user_id))
        {
            sql_query += `AND from_user_id ='${search_data.from_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.from_user_type))
        {
            sql_query += `AND from_user_type ='${search_data.from_user_type}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_id))
        {
            sql_query += `AND to_user_id ='${search_data.to_user_id}' `;
        }
        if(!common_helper.is_empty(search_data.to_user_type))
        {
            sql_query += `AND to_user_type ='${search_data.to_user_type}' `;
        }
        if(!common_helper.is_empty(search_data.status))
        {
            sql_query += `AND status ='${search_data.status}' `;
        }
        if(!common_helper.is_empty(search_data.in_chat))
        {
            sql_query += `AND in_chat ='${search_data.in_chat}' `;
        }

       
        if(!common_helper.is_empty(per_page))
        {
            sql_query += `LIMIT ${page},${per_page}`;
        }
        
       

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

 

async function save(insert_data,request_id = '') 
{
   if(common_helper.is_empty(request_id))
   {    
        insert_data.created_at = common_helper.get_current_date();
        let sql_query = common_helper.get_Insert_Query(insert_data,table) ;
        //console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
   else
   {    insert_data.modified_at = common_helper.get_current_date();
        let sql_query = common_helper.get_Update_Query(insert_data,table,table+'.request_id ='+request_id) ;
       // console.log(sql_query,'sql_query') ;
        return db_sql.query(sql_query);
    }
}
   
async function get_chat_request_id(from_user_id,from_user_type,to_user_id,to_user_type,search_data='') 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0' AND request_type ='2' `;
    sql_query += `AND
    ( 
        ( (from_user_id ='${to_user_id}' AND from_user_type ='${to_user_type}') AND (to_user_id ='${from_user_id}' AND to_user_type = '${from_user_type}') )
      OR ( (from_user_id ='${from_user_id}' AND from_user_type ='${from_user_type}') AND (to_user_id ='${to_user_id}' AND to_user_type ='${to_user_type}'))
   ) AND status =1 `;
        
        sql_query += `LIMIT 0,1`;

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

async function check_chat_request_id(request_id,from_user_id,from_user_type) 
{
    let sql_query = `SELECT *  FROM ${table} WHERE  is_deleted = '0' AND request_type ='2' `;
    sql_query += `AND request_id ='${request_id}' `;
    sql_query += `AND
            ( 
              ( 
               (from_user_id ='${from_user_id}' AND from_user_type ='${from_user_type}') OR (to_user_id ='${from_user_id}' AND to_user_type = '${from_user_type}') 
              )  
            ) AND status ='1' `;
        
        sql_query += `LIMIT 0,1`;

    console.log(sql_query,'sql_query');
    return db_sql.query(sql_query);
}

module.exports = { 
    save,
    get_data_by_params,
    get_chat_request_id,
    check_chat_request_id,

}