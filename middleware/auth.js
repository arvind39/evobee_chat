const Device_sessions_model = require(`../models/device_sessions_model.js`);

module.exports = async (req,res,next) => {
 //console.log('calling');
    try {
        if(req.headers.authorization === undefined || req.headers.authorization == null || req.headers.authorization.length <= 0)
        {

            res.json({
                status:401,
                error:{},
                message:'Token expired no header',
                data:[],
            });

        }
        else
        {

            let  headerToken = req.headers.authorization.split(" ")[1];
            let access_token = headerToken ;
            //console.log(access_token,'access_token');
            const jwt = require('jsonwebtoken');
            const decode_token = jwt.verify(headerToken,'secret');
            console.log(decode_token,'decode_token');
            
            Device_sessions_model.get_device_token_data(decode_token.user_id,decode_token.user_type,decode_token.device_type,1).then(device_res =>{
                if(parseInt(device_res.length))
                {
                    req.client_data = decode_token ;
                    next() ;
                }
                else
                {
                    res.json({
                        status:401,
                        error:{},
                        message:'Token expired',
                        data:[],
                    });
                }
            });

        }
        
        
    }
    catch (error)
    {
        // console.log(req.body,'req');
        // console.log(error,'error');

        let  headerToken = req.headers.authorization.split(" ")[1];
        let access_token = headerToken ;
        //console.log(access_token,'access_token');

        Device_sessions_model.get_device_data_by_access_token(access_token).then( (device_res)=>{
            if(parseInt(device_res.length))
            {
                let row_data = device_res[0] ;
                let decode_token = {
                                    user_id:row_data.user_id,
                                    user_type:row_data.user_type,
                                    device_type:row_data.device_type,
                                    };
                req.client_data = decode_token ;
                next() ;
            }
            else
            {
                res.json({
                    status:401,
                    error:{},
                    message:'Token expired',
                    data:[],
                });
            }

        });
        
    }

};