const jwt = require('jsonwebtoken');
const Common_model = require(`../models/common_model.js`);
const moment = require('moment'); 

function get_current_date (key='current_date_time') 
{  
    let date_string ;   
    if(key == 'current_date_time')
    {
        date_string = moment().format("YYYY-MM-DD HH:mm:s");
    }
    return date_string;
}

function convert_datetime_string_to_timestamp(dateString)
{

    dateTimeParts = dateString.split(' '),
    timeParts = dateTimeParts[1].split(':'),
    dateParts = dateTimeParts[0].split('-'),
    date;
    date = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);
    return date.getTime();
    
}


function is_empty (val) 
{
    return (val === undefined || val == null || val.length <= 0) ? true : false;

}

function convert_to_empty_errors  (rules_field,errors_data)
 {
    for (let property in rules_field) 
        {
            let is_property = errors_data.hasOwnProperty(property);
            if(!is_property)
            {
                errors_data[property] = {message:'',rule:''} ;
            }
        }
        return errors_data ;
}

function get_response_variable ()
{
    let obj = new Object()
    obj.status  = 0 ;
    obj.error   = {} ;
    obj.message = '' ;
    obj.data    = [] ;
    return  {
         status:0,
         error:{},
         message:'',
         data:[],
        } ; 
}

function genrateAuthToken (user_id,user_type,device_token,device_type) 
{
        let toekn = jwt.sign(
                            {user_id:user_id,user_type:user_type,device_token:device_token,device_type:device_type},
                            'secret',{ expiresIn:'15 days'}
                            );
        return toekn ;    
}

function get_Insert_Query (object_data,tabel_name) 
{
        var keys =[];
        var values =[];
        for(var key in object_data) 
        {
            keys.push(key);
            values.push("'"+object_data[key]+"'");
        }
    let sql_query = `INSERT INTO ${tabel_name} (${keys.join(',')}) VALUES (${values.join(',')})` ;
    return sql_query ;
}
function get_Update_Query (object_data,tabel_name,where_query) 
{
        let set_query =''; 
        let i = 0 ;
        for(var key in object_data) 
        {
            set_query += i== 0 ?  `${key} = '${object_data[key]}'` : `, ${key} = '${object_data[key]}'` ;
            i++;
        }
    let sql_query = `UPDATE ${tabel_name} SET ${set_query} WHERE ${where_query}` ;
    return sql_query ;
}

 async function get_state_list_by_country_id(country_id=101) 
{

     let data =   await Common_model.get_state_list_by_country_id(country_id);
      return parseInt(data.length) ? data : [];
}

async function get_city_list_by_state_id(state_id) 
{

     let data =   await Common_model.get_city_list_by_state_id(state_id);
      return parseInt(data.length) ? data : [];
}

function get_ip_address(req) 
{
    const requestIp = require('request-ip');
    let ip_address = requestIp.getClientIp(req);
    //console.log(ip_address,'ip_address'); 
    //var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    //console.log(ip,'ip')
    return ip_address ;
}

async function get_user_data_by_id_type(user_id,user_type) 
{       
    return new Promise( async (resolve,reject)=>{

        Common_model.get_user_data_by_id_type(user_id,user_type).then( (res) => {
            if(parseInt(res.length))
            {
                resolve(res[0]);
            }
            else
            {
                resolve({});
            }
        }).catch( (err)=>{
              console.log(err,'db error')
              resolve({});
        });

     });
}

function millisecondsToStr (milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();
    
    
    function numberEnding (number) {
        return (number > 1) ? 's' : '';
    }

    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
        return years + ' year' + numberEnding(years);
    }
    //TODO: Months! Maybe weeks? 
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
        return days + ' day' + numberEnding(days);
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
        return hours + ' hour' + numberEnding(hours);
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
        return minutes + ' minute' + numberEnding(minutes);
    }
    var seconds = temp % 60;
    if (seconds) {
        return seconds + ' second' + numberEnding(seconds);
    }
    return 'less than a second'; //'just now' //or other string you like;
}
  
function extractValue(arr, prop) {

    let extractedValue = arr.map(item => item[prop]);
    return extractedValue;

}

module.exports = {
    get_current_date,
    is_empty,
    convert_to_empty_errors,
    get_response_variable,
    genrateAuthToken,
    get_Insert_Query,
    get_Update_Query,
    get_state_list_by_country_id,
    get_city_list_by_state_id,
    get_ip_address,
    get_user_data_by_id_type,
    millisecondsToStr,
    extractValue,
  }