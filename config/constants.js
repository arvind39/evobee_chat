const IS_ADMIN = 1 ;
const IS_BUSINESS = 2 ;
const IS_USER = 3 ;
const IS_SUB_BUSINESS_USER = 4 ;
const IS_SUB_ADMIN_USER = 5 ;
const ACTIVITY_LOG = {
                      LOGIN_ITEM_TYPE: 1,
                    } ;

module.exports = {
    IS_ADMIN,
    IS_BUSINESS,
    IS_USER,
    IS_SUB_BUSINESS_USER,
    IS_SUB_ADMIN_USER,
    ACTIVITY_LOG,
  }