  const config_db = { 
          host: process.env.DB_HOST,
          user: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_DATABASE,
          port: process.env.DB_PORT,
      };


  module.exports = config_db;