
const config_db = require('../config/config_db.js');
var mysql = require("mysql");
var db_connection = mysql.createConnection(config_db);
  db_connection.connect(function (error) {
    if (error) {
       console.log('mysql erro: ' + error.message);
    }
    console.log('Connected to the MySQL server.');
});

async function query (sql_query) 
{
  return new Promise( (resolve,reject)=>{
    db_connection.query(sql_query,(error, results, fields) => {
      if (error) 
      {
        reject(error.message);
      }
      else
      {
        resolve(results);
      }
    });
   });
};



module.exports = {
  db_connection,
  query,
}